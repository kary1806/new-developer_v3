from .apps.rules import constants, models


class RulesConditionNotFound(Exception):
    pass


def process_ip_and_payment(payments: list, methods: list, transacion: int, time: int):
    pass


def process_bin_and_payment(payments: list, methods: list, transacion: int, time: int):
    pass


RULES_PAYMENTS_CONDITIONS = {
    constants.RuleType.CHECK_IP: process_ip_and_payment,
    constants.RuleType.CHECK_BIN: process_bin_and_payment,
}


def process_rules(rule: models.Rules, payments: list) -> None:
    methods = rule.payment_method.filter(is_active=True).values_list("name", flat=True)
    if rule.check_type == constants.CheckType.PAYMENT_TYPE:
        if rule.rule_type not in RULES_PAYMENTS_CONDITIONS:
            raise RulesConditionNotFound()
        return RULES_PAYMENTS_CONDITIONS[rule.rule_type](
            payments=payments,
            methods=methods,
            transacion=rule.transacion_limit,
            time=rule.time_range,
        )
