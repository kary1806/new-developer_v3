from solve_point_4 import find_pair


def test_solve_point_4_return_success():
    numbers = [2, 3, 6, 7]
    target_sum = 9
    result = find_pair(numbers=numbers, target_sum=target_sum)

    assert result == (2, 7)


def test_solve_point_4_return_not_success():
    numbers = [1, 3, 3, 7]
    target_sum = 9
    result = find_pair(numbers=numbers, target_sum=target_sum)

    assert result == None
