def find_pair(numbers: list, target_sum: int):
    left = 0
    right = len(numbers) - 1

    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target_sum:
            return (numbers[left], numbers[right])
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1

    return None


numbers = [2, 3, 6, 7]
target_sum = 9
result = find_pair(numbers=numbers, target_sum=target_sum)
if result:
    print(f"OK, matching pair {result}")
else:
    print("No")
