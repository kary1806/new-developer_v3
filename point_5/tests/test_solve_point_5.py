from solve_point_5 import convert_hex_in_text


def test_solve_point_5_return_success():
    data = """4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772
    6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c
    736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374
    612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220
    617175ed212e"""
    result = convert_hex_in_text(data=data)
    hex_string = data
    hex_bytes = bytes.fromhex(hex_string)
    text_expected = hex_bytes.decode("latin-1")

    assert result == text_expected
