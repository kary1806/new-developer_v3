import datetime
import locale
import os

# Obtener la fecha y hora actual
now = datetime.datetime.now()

# Obtener el sistema operativo que se está utilizando
operating_system = os.name

# Obtener los idiomas que se dominan
languages = locale.getdefaultlocale()

# Crear una cadena con la información obtenida
info_str = f"Último uso de RTFM y LMGTFY: {now}\nSistema operativo: {operating_system}\nIdiomas: {languages}"

# Escribir la cadena en un archivo de texto
with open("info.txt", "w") as f:
    f.write(info_str)
